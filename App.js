import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  ScrollView,
  Image,
  PixelRatio,
  Dimensions,
  Animated,
  StatusBar
} from 'react-native';

import { MainNavigator } from './src/helpers/PageStructure';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <MainNavigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
