import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image, 
    TouchableOpacity,
    Animated,
    PanResponder,
    Dimensions,
    Linking
} from 'react-native';

import Communications from 'react-native-communications';

import goBackLogo from '../../assets/images/goBackLogoPixel.png';
import phoneLogo from '../../assets/images/phonePixelLogoNicer.png';
import mailLogo from '../../assets/images/mailPixelLogo.png';
import whatsappLogo from '../../assets/images/whatsappPixelLogoGreen.png';
import dragAndDropLogo from '../../assets/images/dragAndDropImage.png';

import * as colors from '../helpers/CollorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const ICON_CONTAINER_HEIGHT = 90;

const phoneNumber = '+905321538009';
const whatsappText = 'Hello from app!';

import styles from './styles';

class ContactMe extends Component {
    state = {
        animationPhone: new Animated.ValueXY(),
        animationMail: new Animated.ValueXY(),
        animationWhatsapp: new Animated.ValueXY(),
        animationPhoneScale: new Animated.Value(1),
        animationMailScale: new Animated.Value(1),
        animationWhatsappScale: new Animated.Value(1)
    }

    startGrowAnimation = (animation) => {
        Animated.timing(animation, {
            toValue: 1.5,
            duration: 200
        }).start();
    }

    startShrinkAnimation = (animation) => {
        Animated.timing(animation, {
            toValue: 1,
            duration: 200
        }).start();
    }

    phoneCallFunc = () => {
        return (
            Communications.phonecall(phoneNumber, true),
            this.state.animationPhone.setValue({x: 0, y: 0})
        )
    }

    emailSendFunc = () => {
        return (
            Communications.email(['cankatm@gmail.com'],null,null,'From Mobile App',''),
            this.state.animationMail.setValue({x: 0, y: 0})
        )
    }

    whatsappFunc = () => {
        return (
            Linking.openURL(`whatsapp://send?text=${whatsappText}&phone=${phoneNumber}`),
            this.state.animationWhatsapp.setValue({x: 0, y: 0})
        )
    }

    componentWillMount() {
        this._panResponderPhone = PanResponder.create({
            onStartShouldSetPanResponder:(evt, gestureState) => true,
            onMoveShouldSetPanResponder:(evt, gestureState) => true,
            onPanResponderGrant:(e, gestureState) => {
                this.state.animationPhone.extractOffset();
                this.startGrowAnimation(this.state.animationPhoneScale)
            },
            onPanResponderMove: Animated.event([
                null,
                {
                    dx: this.state.animationPhone.x,
                    dy: this.state.animationPhone.y
                }
            ])
            ,
            onPanResponderRelease:(e, gestureState) => {
                this.startShrinkAnimation(this.state.animationPhoneScale)
                if (gestureState.dy >= (WINDOW_HEIGHT / 2) - (WINDOW_HEIGHT / 5) ) {
                    return(
                        this.phoneCallFunc(),
                        console.log('içerde!')
                    )
                }
                this.state.animationPhone.setValue({x: 0, y: 0})
            },
        })

        
        this._panResponderMail = PanResponder.create({
            onStartShouldSetPanResponder:(evt, gestureState) => true,
            onMoveShouldSetPanResponder:(evt, gestureState) => true,
            onPanResponderGrant:(e, gestureState) => {
                this.state.animationMail.extractOffset();
                this.startGrowAnimation(this.state.animationMailScale)
            },
            onPanResponderMove: Animated.event([
                null,
                {
                    dx: this.state.animationMail.x,
                    dy: this.state.animationMail.y
                }
            ])
            ,
            onPanResponderRelease:(e, gestureState) => {
                this.startShrinkAnimation(this.state.animationMailScale)
                if (gestureState.dy >= (WINDOW_HEIGHT / 2) - (WINDOW_HEIGHT / 5) ) {
                    return(
                        this.emailSendFunc()
                    )
                }
                this.state.animationMail.setValue({x: 0, y: 0})
            },
        })

        
        this._panResponderWhatsapp = PanResponder.create({
            onStartShouldSetPanResponder:(evt, gestureState) => true,
            onMoveShouldSetPanResponder:(evt, gestureState) => true,
            onPanResponderGrant:(e, gestureState) => {
                this.state.animationWhatsapp.extractOffset();
                this.startGrowAnimation(this.state.animationWhatsappScale)
            },
            onPanResponderMove: Animated.event([
                null,
                {
                    dx: this.state.animationWhatsapp.x,
                    dy: this.state.animationWhatsapp.y
                }
            ])
            ,
            onPanResponderRelease:(e, gestureState) => {
                this.startShrinkAnimation(this.state.animationWhatsappScale)
                if (gestureState.dy >= (WINDOW_HEIGHT / 2) - (WINDOW_HEIGHT / 5) ) {
                    return(
                        this.whatsappFunc()
                    )
                }
                this.state.animationWhatsapp.setValue({x: 0, y: 0})
            },
        })
    }

    render() {
        const animatedPhoneStyles = {
            transform: [
                ...this.state.animationPhone.getTranslateTransform(),
                {
                    scale: this.state.animationPhoneScale
                }
            ]
        }

        const animatedMailStyles = {
            transform: [
                ...this.state.animationMail.getTranslateTransform(),
                {
                    scale: this.state.animationMailScale
                }
            ]
        }

        const animatedWhatsappStyles = {
            transform: [
                ...this.state.animationWhatsapp.getTranslateTransform(),
                {
                    scale: this.state.animationWhatsappScale
                }
            ]
        }

        return (
            <View style={{ flex: 1, backgroundColor: colors.orangePastel }} >

                <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                    <Image source={goBackLogo} style={{ width: 100, height: 50, resizeMode: 'contain' }} />
                </TouchableOpacity>

                <View style={{ width: WINDOW_WIDTH ,position: 'absolute', bottom: (WINDOW_HEIGHT / 2), left: 0, alignItems: 'center', justifyContent: 'center' }} >
                    <Image source={dragAndDropLogo} style={{ width: WINDOW_WIDTH / 2, height: WINDOW_WIDTH / 8, resizeMode: 'contain' }} />
                </View>

                <View style={styles.contactPageDraggableAreaContainerStyle} >
                </View>

                <Animated.View 
                    style={[styles.contactPageDraggableContainerStyle, { left: (WINDOW_WIDTH / 5) - (ICON_CONTAINER_HEIGHT / 2)}, animatedPhoneStyles]} 
                    { ...this._panResponderPhone.panHandlers }
                >
                    <Image source={phoneLogo} style={styles.contactPageLogoImageStyle} />
                </Animated.View>

                <Animated.View 
                    style={[styles.contactPageDraggableContainerStyle, { left: (WINDOW_WIDTH / 2) - (ICON_CONTAINER_HEIGHT / 2)}, animatedMailStyles]} 
                    { ...this._panResponderMail.panHandlers }
                >
                    <Image source={mailLogo} style={styles.contactPageLogoImageStyle} />
                </Animated.View>

                <Animated.View 
                    style={[styles.contactPageDraggableContainerStyle, { right: (WINDOW_WIDTH / 5) - (ICON_CONTAINER_HEIGHT / 2)}, animatedWhatsappStyles]} 
                    { ...this._panResponderWhatsapp.panHandlers }
                >
                    <Image source={whatsappLogo} style={styles.contactPageLogoImageStyle} />
                </Animated.View>
            </View>
        );
    }
}

export default ContactMe;