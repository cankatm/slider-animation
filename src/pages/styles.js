import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../helpers/CollorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const ICON_CONTAINER_HEIGHT = 90;

export default StyleSheet.create({
    openingScreenContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black
    },
    contactPageDraggableAreaContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: WINDOW_HEIGHT / 2, 
        position: 'absolute', 
        bottom: 0, 
        left: 0, 
        borderTopWidth: 1,
        borderColor: 'purple', 
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: colors.bluePastel,
    },
    contactPageDraggableContainerStyle: { 
        position: 'absolute', 
        top: WINDOW_HEIGHT / 6, 
        width: ICON_CONTAINER_HEIGHT, 
        height: ICON_CONTAINER_HEIGHT, 
        borderRadius: ICON_CONTAINER_HEIGHT / 2,
        alignItems: 'center', 
        justifyContent: 'center',
    },
    contactPageLogoImageStyle: { 
        width: (ICON_CONTAINER_HEIGHT / 3) * 2,
        height: (ICON_CONTAINER_HEIGHT / 3) * 2,
        resizeMode: 'contain'
    },
});