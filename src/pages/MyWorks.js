import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity
} from 'react-native';

import styles from './styles';

class MyWorks extends Component {
    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                    <Text>Go Back</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default MyWorks;