import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  ScrollView,
  Image,
  PixelRatio,
  Dimensions,
  Animated,
  TouchableOpacity
} from 'react-native';

import MakeItRain from '../components/MakeItRain';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

import * as colors from '../helpers/CollorPalette';

import worksButton from '../../assets/images/works-button2.png';
import contactButton from '../../assets/images/contact-button2.png';
import rainButton from '../../assets/images/rain-button.png';
import rainCancelButton from '../../assets/images/rain-cancel-button.png';
import swipeRight from '../../assets/images/swipe-right.png';
import swipeLeft from '../../assets/images/swipe-left.png';
import cloudPixel1 from '../../assets/images/cloud1.png';
import cloudPixel2 from '../../assets/images/cloud2.png';
import cloudPixel3 from '../../assets/images/cloud3.png';
import cloud1 from '../../assets/images/cloudReal1.png';
import cloud2 from '../../assets/images/cloudReal2.png';
import cloud3 from '../../assets/images/cloudReal3.png';
import cloud4 from '../../assets/images/cloudReal4.png';
import sun from '../../assets/images/sun2.png';
import sunPastel from '../../assets/images/sun-pastel2.png';
import moonPastel from '../../assets/images/moon-pastel2.png';
import alyen from '../../assets/images/plain.png';
import bird from '../../assets/images/bird.gif';
import bird2 from '../../assets/images/bird2.gif';
import bird3 from '../../assets/images/bird3.gif';
import bird4 from '../../assets/images/bird4.gif';
import bird5 from '../../assets/images/birdPixel2.gif';

const { width, height } = Dimensions.get('window');
const decWidth = width / 10;
const decHeight = height / 10;

const getScreen1Styles = (animation, width) => {
  const cloud1TranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, -200],
    extrapolate: 'clamp'
  })

  const cloud2TranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [width, decWidth * 3],
    extrapolate: 'clamp'
  })

  const cloud3TranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, -60],
    extrapolate: 'clamp'
  })

  const cloud4TranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, -80],
    extrapolate: 'clamp'
  })

  const alyenTranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, width - 150],
    extrapolate: 'clamp'
  })

  const alyenTranslateY = animation.interpolate({
    inputRange: [0, width / 4, width / 2, (width / 4) * 3, width],
    outputRange: [0, 0, 100, 220, 220],
    extrapolate: 'clamp'
  })

  const alyenRotation = animation.interpolate({
    inputRange: [0, width / 4, width / 2, (width / 4) * 3, width],
    outputRange: ['0deg', '0deg', '45deg', '0deg', '0deg'],
    extrapolate: 'clamp'
  })

  return {
    cloud1: {
      transform: [{
        translateX: cloud1TranslateX
      }]
    },
    cloud2: {
      transform: [{
        translateX: cloud2TranslateX
      }]
    },
    cloud3: {
      transform: [{
        translateX: cloud3TranslateX
      }]
    },
    cloud4: {
      transform: [{
        translateX: cloud4TranslateX
      }]
    },
    alyen: {
      transform: [
        {
          translateX: alyenTranslateX
        },
        {
          translateY: alyenTranslateY
        },
        {
          rotate: alyenRotation
        },
      ]
    },
  }
}

export default class SliderPage extends Component {
  state = {
    animation: new Animated.Value(0),
    isRaining: false
  }

  render() {
    const screen1Styles = getScreen1Styles(this.state.animation, width);

    const sliderBackgroundInterpolate = this.state.animation.interpolate({
      inputRange: [0, width / 2, width],
      outputRange: [colors.orangePastel, colors.orangePastel, colors.bluePastel],
      extrapolate: 'clamp'
    })

    const renderRainButton = () => {
      if (!this.state.isRaining) {
        return <Image source={rainButton} style={{ width: 48, height: 48, resizeMode: 'contain' }} />
      }

      return <Image source={rainCancelButton} style={{ width: 48, height: 48, resizeMode: 'contain' }} />
    }

    const renderRain = () => {
      if (this.state.isRaining) {
        return <MakeItRain />;
      }
    }

    return (
      <View style={styles.container}>
      //CLOUDS
      <Animated.Image 
        source={cloudPixel1}
        style={[{
          width: PixelRatio.getPixelSizeForLayoutSize(40),
          height: PixelRatio.getPixelSizeForLayoutSize(20),
          position: 'absolute',
          top: decHeight * 6,
          left: decWidth,
          zIndex: 20
        },
        screen1Styles.cloud1
        ]}
        resizeMode= 'contain'
      />

      <Animated.Image 
        source={cloudPixel2}
        style={[
          {
            width: PixelRatio.getPixelSizeForLayoutSize(50),
            height: PixelRatio.getPixelSizeForLayoutSize(25),
            position: 'absolute',
            top: decHeight * 6,
            left: decWidth * 3,
            zIndex: 20
          },
          screen1Styles.cloud2
        ]}
        resizeMode= 'contain'
      />

      <Animated.Image 
        source={cloudPixel2}
        style={[
          {
            width: PixelRatio.getPixelSizeForLayoutSize(40),
            height: PixelRatio.getPixelSizeForLayoutSize(20),
            position: 'absolute',
            top: decHeight * 4,
            left: decWidth * 2,
            zIndex: 20
          },
          screen1Styles.cloud4
        ]}
        resizeMode= 'contain'
      />
        <AnimatedScrollView
          style={[styles.scontainer, { backgroundColor: sliderBackgroundInterpolate}]}
          pagingEnabled
          horizontal
          scrollEventThrottle={1}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.state.animation
                  }
                }
              }
            ]
          )}
        >
          
          <View style={{ width, height, backgroundColor: 'transparent' }} >

            <View style={{ position: 'absolute', right: 32, top: 64}} >
                  <Image source={swipeRight} style={{ width: 120, height: 60, resizeMode: 'contain' }} />
            </View>

            <View style={styles.screenHeader} >
              <Animated.Image 
                source={sunPastel}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(75),
                  height: PixelRatio.getPixelSizeForLayoutSize(63),
                }}
                resizeMode= 'contain'
              />
            </View>

            <View style={{ position: 'absolute', bottom: 100, left: width / 2 - 60}} >
                <TouchableOpacity onPress={() => this.props.navigation.navigate('MyWorks')} >
                  <View style={{ width: 120, height: 60, backgroundColor: 'white', borderWidth: 1, borderColor: '#000' }} >
                    <Image source={worksButton} style={{ width: 120, height: 60, resizeMode: 'contain' }} />
                  </View>
                </TouchableOpacity>
            </View>
          </View>
          
          <View style={{ width, height, backgroundColor: 'transparent' }} >

            <View style={{ position: 'absolute', right: 32, top: 64}} >
              <Image source={swipeLeft} style={{ width: 120, height: 60, resizeMode: 'contain' }} />
            </View>

            <View style={styles.screenHeader} >
              <Animated.Image 
                source={moonPastel}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(75),
                  height: PixelRatio.getPixelSizeForLayoutSize(63),
                }}
                resizeMode= 'contain'
              />
            </View>

            <View style={{ position: 'absolute', bottom: 100, left: width / 2 - 60}} >
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactMe')} >
                  <View style={{ width: 120, height: 60, backgroundColor: 'white', borderWidth: 1, borderColor: '#000' }} >
                    <Image source={contactButton} style={{ width: 120, height: 60, resizeMode: 'contain' }} />
                  </View>
                </TouchableOpacity>
            </View>
          </View>

        </AnimatedScrollView>


        //BİRDS
        <Animated.Image 
              source={bird5}
              style={[
                {
                  width: PixelRatio.getPixelSizeForLayoutSize(12),
                  height: PixelRatio.getPixelSizeForLayoutSize(12),
                  position: 'absolute',
                  top: decHeight * 3,
                  left: decWidth * 1,
                  zIndex: 100
                },
                screen1Styles.alyen
              ]}
              resizeMode= 'contain'
            />

        {renderRain()}

        //RAINING
        <View style={{ position: 'absolute', top: 50, left: 50}} >
            <TouchableOpacity onPress={() => this.setState({ isRaining: !this.state.isRaining})} >
              {renderRainButton()}
            </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scontainer: {
    flex: 1,
  },
  screenHeader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});
