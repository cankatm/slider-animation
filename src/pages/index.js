import SliderPage from './SliderPage';
import MakeItRainAgain from './MakeItRainAgain';
import MyWorks from './MyWorks';
import ContactMe from './ContactMe';
import styles from './styles';

export { 
    SliderPage, 
    MakeItRainAgain, 
    MyWorks, 
    ContactMe, 
    styles 
};