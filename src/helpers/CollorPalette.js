export const blueKuark = '#4E76BC';
export const darkBlueKuark = '#191B1D';
export const white = '#fff';
export const darkestGrey = '#333';
export const black = '#000';

export const orangePastel = '#E86E47';
export const whitePastel = '#EECEAD';
export const bluePastel = '#789DA3';