import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, Easing, StatusBar } from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';

import * as colors from './CollorPalette';
import { 
    SliderPage, 
    MakeItRainAgain, 
    MyWorks, 
    ContactMe, 
} from '../pages';

export const MainNavigator = StackNavigator({
        SliderPage: { screen: SliderPage },
        MakeItRainAgain: { screen: MakeItRainAgain },
        MyWorks: { screen: MyWorks },
        ContactMe: { screen: ContactMe },
    },
    {
        headerMode: 'none',
        lazyLoad: true,
        navigationOptions: {
            gesturesEnabled: false
        }
    }
);